grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | localBlock)+ EOF!; // wykrzyknik oznacza, że element nie trafi do drzewa

localBlock
    : LB^ (stat)* RB
;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | NL -> // puste drzewo przy pustej linii
    ;

expr
    : multExpr
    ( PLUS^ multExpr
    | MINUS^ multExpr
    )*
    ;

multExpr
    : powExpr
    ( MUL^ powExpr
    | DIV^ powExpr
    )*
    ;
    
powExpr
    : logicalExpr
    ( POW^ powExpr
    )?
    ;    

logicalExpr
    : atom
    ( OR^ atom
    | XOR^ atom
    | AND^ atom
    | MORE^ atom
    | LESS^ atom
    )*
  ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    
PRINT : 'print';    

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP : '(';
RP : ')';

LB : '{';
RB : '}';

PODST	: '=';

PLUS :	'+';
MINUS	: '-';

MUL	: '*';
POW : '^';
DIV	: '/';	
MOD : '%';
  
AND : '&&';  
OR : '||';  
XOR : '<>'; 

MORE : '>';
LESS : '<';
