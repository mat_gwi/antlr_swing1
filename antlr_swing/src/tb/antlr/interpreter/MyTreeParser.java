package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols; 

public class MyTreeParser extends TreeParser {

	private Integer scopeLevel = 0;
	
	private GlobalSymbols globalSymbols = new GlobalSymbols();

	private LocalSymbols localSymbols = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer sum(Integer a, Integer b) {
		return a+b;
	}	

	protected Integer substract(Integer a, Integer b) {
		return a-b;
	}	

	protected Integer multiply(Integer a, Integer b) {
		return a*b;
	}	

	protected Integer divide(Integer a, Integer b) throws RuntimeException {
		if(b == 0) throw new RuntimeException("Can't divide by 0.");		

		return a / b;
	}	

	protected Integer modulo(Integer a, Integer b) throws RuntimeException {
		if(b == 0) throw new RuntimeException("Can't divide by 0.");		

		return a % b;
	}
	
	protected Integer power(Integer a, Integer b){
		return (int)Math.pow(a, b);
	}
	
	protected Integer logicalAnd(Integer x, Integer y){ 
		return x & y;
	}
	
	protected Integer logicalOr(Integer x, Integer y){ 
		return x | y;
	}

	protected Integer logicalXor(Integer x, Integer y){ 
		return x ^ y;
	}
	
	protected Integer logicalMore(Integer x, Integer y){ 
		return x > y ? 1 : 0;
	}

	protected Integer logicalLess(Integer x, Integer y){ 
		return x < y ? 1 : 0;
	}
	
	protected void createVariable(String name) {
		if (scopeLevel>=1) localSymbols.newSymbol(name);
		else globalSymbols.newSymbol(name);
	}	

	protected Integer setVariable(String name, Integer value) {
		if (scopeLevel>=1) localSymbols.setSymbol(name, value);
		else globalSymbols.setSymbol(name, value);
		return value;
	}

	protected Integer getVariable(String name) { 
		if (scopeLevel>=1) return localSymbols.getSymbol(name);
		else return globalSymbols.getSymbol(name);
	}	

	protected void enterLocalBlock() { 
		localSymbols.enterScope();
		scopeLevel = scopeLevel + 1 ; 
	}
	
	protected void exitLocalBlock() { 
		localSymbols.leaveScope();
		scopeLevel = scopeLevel - 1 ; 
	}
}
