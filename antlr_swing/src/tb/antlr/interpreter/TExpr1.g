tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog : (print | e=expr | declareVariable | localBlock)*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = sum($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = substract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}
        | ^(AND   e1=expr e2=expr) {$out = logicalAnd($e1.out, $e2.out);}
        | ^(OR    e1=expr e2=expr) {$out = logicalOr($e1.out, $e2.out);}
        | ^(XOR   e1=expr e2=expr) {$out = logicalXor($e1.out, $e2.out);}
        | ^(MORE  e1=expr e2=expr) {$out = logicalMore($e1.out, $e2.out);}
        | ^(LESS  e1=expr e2=expr) {$out = logicalLess($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = setVariable($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVariable($ID.text);}
        ;

print : ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());};

declareVariable : ^(VAR i1=ID) {createVariable($i1.text);};

localBlock 
        : LB {enterLocalBlock();}
        | RB {exitLocalBlock();}
        ;